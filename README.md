```
==== API doopravdy  ====
!!! vše kromě POST na /account/ je autorizované přes HTTP BASIC !!!

Nepoužívám žádnej api tester, takže neporadim, requesty dělám z shellu přes cURL, např:
$ curl -X DELETE -i -u testuser:123456 http://localhost:8080/account/1

přihlášení: při každém requestu (viz jak funguje HTTP BASIC). Pokud přihlášení neproběhne, vrátí 401
(uživatelé jsou zatím pouze v paměti)
Autorizace & autentifikace:

POST /account/login slouží jen k ověření přihlášení, vrací 200 + detaily uživatele nebo 401 fail
/account/logout neexistuje (REST je bezstavovej, neexistuje session a v HTTP BASIC se ani odhlásit nejde)

Správa účtu:
POST /account/ registruje uživatele, přijímá username a password. Vrací 201 při úspěchu, 409 když uživatel už existuje.
GET /account/{ID} vrací detaily účtu uživatele. POZOR, povolí jen ID toho kdo je přihlášenej, nemůžeme si GETnout cizí účet. 200 při úspěchu, jinak 403
PUT /account/{ID} mění heslo, přijímá parametr "new_password". Vrací 204 při úspěchu, 403 pokud bych to zkoušel na špatnýho uživatele
DELETE /account/{ID} mázne účet. Vrací 204 při úspěchu, 403 pokud bych to zkoušel na špatnýho uživatele.

Filesystém:
Pro upřesnění, ty requesty jsou trochu jiný než jsme původně psali, zejména co se týká adres. Co je důležitý vědět je, že každá věc má tzv. "object id".
Je to v 32 znaků dlouhej alnum řetězec.

GET /filesystem/{ID UŽIVATELE} kořenová složka uživatele (200, 404, 403)
GET /filesystem/{ID UŽIVATELE}/{OBJECT ID} vrací složku (200, 404, 403)
GET /filesystem/{ID UŽIVATELE}/f{OBJECT ID} vrací soubor (pozor na to "f") (200, 404, 403)
GET /filesystem/{ID UŽIVATELE}/{OBJECT ID SLOŽKY}/f{OBJECT ID} to samý, akorát v zanořený složce

Vytváření: oba requesty pokud dostanou parametr "name" tak vytvořej složku, pokud soubor "file" vytvořej soubor.
Musej dostat buď jedno nebo druhý jinak 400 Bad Request
POST /filesystem/{ID UŽIVATELE}, parametr "name" vytvoří objekt v kořenovym adresáři (400, 201)
POST /filesystem/{ID UŽIVATELE}/{OBJECT ID}, vytvoří objekt v adresáři podle ID (400, 201, 404 pokud adresář neexistuje)

DELETE /filesystemm/{ID UŽIVATELE}/f{OBJECT ID} mázne soubor (204, 404)
DELETE /filesystemm/{ID UŽIVATELE}/{OBJECT ID SLOŽKY}/f{OBJECT ID} to samý

DELETE /filesystem/{ID UŽIVATELE}/{OBJECT ID} maže složku. Bere to parametr "recursive" - pokud neni nebo je false, maže to jen prázdný složky, s true maže "rm -rf" stylem

PUT /filesystem/{ID UŽIVATELE}/f{OBJECT ID} provádí "rename and move" nad souborem. Bere parametry "name" a "parentDirectory" (parentDirectory vynechat pro kopírování do home složky), vrací 200 a json s infem o souboru
PUT /filesystem/{ID UŽIVATELE}/{OBJECT ID SLOŽKY}/f{OBJECT ID} to samý

PUT /filesystem/{ID UŽIVATELE}/{OBJECT ID} rename složky, bere parametr "name" jako novej název. Move složky jsem nedělal, páč se mi nechtělo řešit cirkulární reference atd, tak kdybyste to někdo JO potřeboval, feel free :D
```