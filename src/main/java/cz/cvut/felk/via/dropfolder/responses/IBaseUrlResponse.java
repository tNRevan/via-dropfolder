package cz.cvut.felk.via.dropfolder.responses;

/**
 * Interface adding base url to responses.
 * @author Petr Kalivoda
 */
public interface IBaseUrlResponse extends IResponse {
    public void setBaseUrl(String baseURL);    
}
