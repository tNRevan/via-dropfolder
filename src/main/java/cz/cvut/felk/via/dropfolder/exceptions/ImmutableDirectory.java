package cz.cvut.felk.via.dropfolder.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception to be thrown while attempting to edit/delete an immutable directory
 * (eg. root directory)
 *
 * @author Petr Kalivoda
 */
@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
public class ImmutableDirectory extends RuntimeException {
    public ImmutableDirectory() {
        super("Requested directory is immutable.");
    }
}
