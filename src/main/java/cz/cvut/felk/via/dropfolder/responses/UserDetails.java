package cz.cvut.felk.via.dropfolder.responses;

import cz.cvut.felk.via.dropfolder.security.User;

/**
 * Response containing user details.
 * @author Petr Kalivoda
 */
public class UserDetails implements IBaseUrlResponse {
    
    private final User user;
    
    private String baseURL = "";
    
    public UserDetails(User u) {
        user = u;
    }
    
    @Override
    public void setBaseUrl(String baseURL) {
        this.baseURL = baseURL;
    }
    
    public int getId() {
        return user.getID();
    }
    
    public String getUsername() {
        return user.getUsername();
    }
    
    @Override
    public String getUrl() {
        return String.format("%s/account/%d", baseURL, user.getID());
    }
    
    public String getFilesystemUrl() {
        return String.format("%s/filesystem/%d", baseURL, user.getID());
    }
}
