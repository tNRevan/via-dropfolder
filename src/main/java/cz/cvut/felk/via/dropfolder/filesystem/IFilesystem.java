package cz.cvut.felk.via.dropfolder.filesystem;

import cz.cvut.felk.via.dropfolder.exceptions.Conflict;
import cz.cvut.felk.via.dropfolder.exceptions.DirectoryNotEmpty;
import cz.cvut.felk.via.dropfolder.exceptions.DirectoryNotFound;
import cz.cvut.felk.via.dropfolder.exceptions.FileNotFound;
import cz.cvut.felk.via.dropfolder.exceptions.ImmutableDirectory;
import org.springframework.web.multipart.MultipartFile;

/**
 * Interface for filesystem operations
 *
 * @author Petr Kalivoda
 */
public interface IFilesystem {

    /**
     * Returns new directory name for an account/dirname combination.
     *
     * @param account_id
     * @param realName
     * @return
     */
    public String getDirectoryName(int account_id, String realName);

    /**
     * Returns new file name for account/filename combination
     *
     * @param account_id
     * @param realName
     * @return
     */
    public String getFileName(int account_id, String realName);

    /**
     * Determines wether or not the directory exists.
     *
     * @param account_id
     * @param directoryHash
     * @return
     */
    public boolean directoryExists(int account_id, String directoryHash);

    /**
     * Creates a directory
     *
     * @param account_id
     * @param directoryName
     * @param parentDirectory can be null for home directory
     * @throws DirectoryNotFound if parent directory doesn't exist
     * @throws Conflict if the directory already exists
     * @return
     */
    public DirectoryDescriptor createDirectory(int account_id, String directoryName, String parentDirectory) throws DirectoryNotFound, Conflict;

    /**
     *
     * @param account_id
     * @param directoryHash
     * @return
     * @throws DirectoryNotFound
     */
    public DirectoryDescriptor getDirectory(int account_id, String directoryHash) throws DirectoryNotFound;

    /**
     * Creates a file
     *
     * @param account_id
     * @param file
     * @param parentDirectory can be null for conflict
     * @return
     * @throws DirectoryNotFound
     * @throws Conflict
     */
    public FileDescriptor createFile(int account_id, MultipartFile file, String parentDirectory) throws DirectoryNotFound, Conflict;

    /**
     * Checks if a file exists.
     *
     * @param account_id
     * @param fileHash
     * @return
     */
    public boolean fileExists(int account_id, String fileHash);
    
    /**
     * Returns a file
     * @param account_id
     * @param fileHash
     * @param parentDirectory
     * @return
     */
    public FileDescriptor getFile(int account_id, String fileHash, String parentDirectory) throws FileNotFound;
    
    /**
     * Deletes a file.
     * @param account_id
     * @param fileHash
     * @param parentDirectory
     * @throws FileNotFound 
     */
    public void deleteFile(int account_id, String fileHash, String parentDirectory) throws FileNotFound;
    
    /**
     * Deletes a directory with recursive option.
     * 
     * @param account_id
     * @param directoryHash
     * @param recursive
     * @throws DirectoryNotFound 
     * @throws DirectoryNotEmpty
     * @throws ImmutableDirectory
     */
    public void deleteDirectory(int account_id, String directoryHash, boolean recursive) throws DirectoryNotFound, DirectoryNotEmpty, ImmutableDirectory;
    
    /**
     * Deletes the whole account.
     * @param account_id 
     */
    public void deleteAccount(int account_id);
    
    /**
     * Renames a file.
     * @param account_id
     * @param fileHash
     * @param directoryHash
     * @param newName
     * @throws FileNotFound
     */
    public void renameFile(int account_id, String fileHash, String directoryHash, String newName) throws FileNotFound, Conflict;
    
    /**
     * Moves a file.
     * @param account_id
     * @param fileHash
     * @param directoryHash
     * @param newDirectoryHash 
     * @throws FileNotFound
     */
    public void moveFile(int account_id, String fileHash, String directoryHash, String newDirectoryHash) throws FileNotFound, DirectoryNotFound, Conflict;
    
    /**
     * 
     * @param account_id
     * @param directoryHash
     * @param newName
     * @throws DirectoryNotFound 
     */
    public void renameDirectory(int account_id, String directoryHash, String newName) throws DirectoryNotFound, Conflict;
}
