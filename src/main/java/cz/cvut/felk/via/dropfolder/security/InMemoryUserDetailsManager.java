package cz.cvut.felk.via.dropfolder.security;

import java.util.HashMap;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Void user details manager just to return the right bloody user :(
 * @author Petr Kalivoda
 */
public class InMemoryUserDetailsManager implements IIDSearchableUserDetailsManager  {
    
    private HashMap<String, User> users = new HashMap<String, User>();

    @Override
    public void createUser(UserDetails ud) {
        User u = ud instanceof User ? (User)ud : new InMemoryUser(ud.getUsername(), ud.getPassword(), ud.getAuthorities());
        createUser(u);
    }
        
    public void createUser(User ud) {
        String username = ud.getUsername().toLowerCase();
        if(!userExists(username)) {
            users.put(username, ud);
        }
    }

    @Override
    public void updateUser(UserDetails ud) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteUser(String string) {
        users.remove(string.toLowerCase());
    }

    @Override
    public void changePassword(String oldPassword, String newPassword) {
        Authentication currentUser = SecurityContextHolder.getContext().getAuthentication();

        if (currentUser == null) {
            throw new AccessDeniedException("Can't change password, no logged user found.");
        }

        String username = currentUser.getName();
        users.get(username).setPassword(newPassword);
    }

    @Override
    public boolean userExists(String string) {
        return users.containsKey(string.toLowerCase());
    }

    @Override
    public UserDetails loadUserByUsername(String string) throws UsernameNotFoundException {
        if(!userExists(string)) {
            throw new UsernameNotFoundException(String.format("User \"%s\" does not exist.", string));
        }

        return users.get(string.toLowerCase());
    }
     
    @Override
    public UserDetails loadUserById(int ID) throws UserIDNotFoundException {
        for(User ud : users.values()) {
            if(ud.getID() == ID) {
                return ud;
            }
        }
        
        throw new UserIDNotFoundException(ID);
    }

    @Override
    public User createUser(String userName, String password) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}