package cz.cvut.felk.via.dropfolder.controllers;

import cz.cvut.felk.via.dropfolder.exceptions.BadRequest;
import cz.cvut.felk.via.dropfolder.responses.Directory;
import cz.cvut.felk.via.dropfolder.responses.File;
import cz.cvut.felk.via.dropfolder.responses.IFilesystemEntryResponse;
import cz.cvut.felk.via.dropfolder.responses.ListedEntry;
import cz.cvut.felk.via.dropfolder.filesystem.DirectoryDescriptor;
import cz.cvut.felk.via.dropfolder.filesystem.FileDescriptor;
import cz.cvut.felk.via.dropfolder.filesystem.IFilesystem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * Controller for filesystem operations
 *
 * @author Petr Kalivoda
 */
@RestController
public class Filesystem extends Base {

    protected final IFilesystem filesystem;

    @Autowired
    public Filesystem(IFilesystem filesystem) {
        this.filesystem = filesystem;
    }

    /**
     * Returns a home directory
     *
     * @param account_id
     * @return
     */
    @RequestMapping(value = "/filesystem/{account_id:[0-9]+}", method = RequestMethod.GET)
    public Directory getDirectoryListing(@PathVariable("account_id") int account_id) {
        checkRights(account_id);
        Directory d = new Directory(filesystem.getDirectory(account_id, null));
        d.setBaseUrl(getBaseUrl());
        
        return d;
    }

    /**
     * Returns any other directory In this case, the directory parameter is a
     * md5 encoded string, not a path.
     *
     * RIGHT: /filesystem/0/72b302bf297a228a75730123efef7c41 WRONG:
     * /filesystem/0/foo/bar/wooot
     *
     * @param account_id
     * @param directory
     * @return
     */
    @RequestMapping(value = "/filesystem/{account_id:[0-9]+}/{directory:[a-zA-Z0-9]{32}}", method = RequestMethod.GET)
    public Directory getDirectoryListing(@PathVariable("account_id") int account_id, @PathVariable("directory") String directory) {
        checkRights(account_id);
        Directory d = new Directory(filesystem.getDirectory(account_id, directory));
        d.setBaseUrl(getBaseUrl());
        
        return d;
    }

    /**
     * Returns a file in some subfolder
     *
     * @param account_id
     * @param file
     * @param directory
     * @return
     */
    @RequestMapping(value = "/filesystem/{account_id:[0-9]+}/{directory:[a-zA-Z0-9]{32}}/f{file:[a-zA-Z0-9]{32}}", method = RequestMethod.GET)
    public File getFile(@PathVariable("account_id") int account_id, @PathVariable("file") String file, @PathVariable("directory") String directory) {
        checkRights(account_id);
        File f = new File(filesystem.getFile(account_id, file, directory));
        f.setBaseUrl(getBaseUrl());
        return f;
    }

    /**
     * Returns a file in root folder.
     *
     * @param account_id
     * @param file
     * @return
     */
    @RequestMapping(value = "/filesystem/{account_id:[0-9]+}/f{file:[a-zA-Z0-9]{32}}", method = RequestMethod.GET)
    public File getFile(@PathVariable("account_id") int account_id, @PathVariable("file") String file) {
        checkRights(account_id);
        File f = new File(filesystem.getFile(account_id, file, null));
        f.setBaseUrl(getBaseUrl());
        return f;
    }

    /**
     * Deletes a file in root folder.
     *
     * @param account_id
     * @param file
     * @return
     */
    @RequestMapping(value = "/filesystem/{account_id:[0-9]+}/f{file:[a-zA-Z0-9]{32}}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteFile(@PathVariable("account_id") int account_id, @PathVariable("file") String file) {
        checkRights(account_id);
        filesystem.deleteFile(account_id, file, null);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

    /**
     * Deletes a file in nested folder.
     *
     * @param account_id
     * @param file
     * @param directory
     * @return
     */
    @RequestMapping(value = "/filesystem/{account_id:[0-9]+}/{directory:[a-zA-Z0-9]{32}}/f{file:[a-zA-Z0-9]{32}}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteFile(@PathVariable("account_id") int account_id, @PathVariable("file") String file, @PathVariable("directory") String directory) {
        checkRights(account_id);
        filesystem.deleteFile(account_id, file, directory);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

    /**
     * Deletes a directory
     *
     * @param account_id
     * @param directory
     * @param recursive
     * @return
     */
    @RequestMapping(value = "/filesystem/{account_id:[0-9]+}/{directory:[a-zA-Z0-9]{32}}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteDirectory(@PathVariable("account_id") int account_id, @PathVariable("directory") String directory, @RequestParam(value = "recursive", required = false, defaultValue = "true") boolean recursive) {
        checkRights(account_id);
        filesystem.deleteDirectory(account_id, directory, recursive);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

    /**
     * Method to create a directory in home
     *
     * @param account_id
     * @param name
     * @param parentDirectory
     * @return
     */
    private Directory createDirectory(int account_id, String name, String parentDirectory) {
        return  new Directory(filesystem.createDirectory(account_id, name, parentDirectory));
    }

    /**
     * Creates a file
     *
     * @param account_id
     * @param file
     * @param parentDirectory
     * @return
     */
    private File createFile(int account_id, MultipartFile file, String parentDirectory) {
        return new File(filesystem.createFile(account_id, file, parentDirectory));
    }

    /**
     * Creates a new file or directory based on parameters
     *
     * @param account_id
     * @param name for directory
     * @param file for file
     * @return
     */
    @RequestMapping(value = "/filesystem/{account_id:[0-9]+}", method = RequestMethod.POST)
    public ResponseEntity<ListedEntry> createObject(@PathVariable("account_id") int account_id, @RequestParam(value = "name", required = false) String name, @RequestParam(value = "file", required = false) MultipartFile file) {
        checkRights(account_id);
        if (!(name == null ^ file == null)) {
            throw new BadRequest();
        }

        IFilesystemEntryResponse response;

        if (name == null) {
            response = createFile(account_id, file, null);
        } else {
            response = createDirectory(account_id, name, null);
        }

        response.setBaseUrl(getBaseUrl());
        HttpHeaders headers = new HttpHeaders();
        headers.set("Location", response.getUrl());

        return new ResponseEntity<ListedEntry>(new ListedEntry(response), headers, HttpStatus.CREATED);
    }

    /**
     * Creates new file/directory in some folder other than home based on
     * parameters.
     *
     * @param account_id
     * @param name
     * @param file
     * @param parentDirectory
     * @return
     */
    @RequestMapping(value = "/filesystem/{account_id:[0-9]+}/{directory:[0-9a-zA-Z]{32}}", method = RequestMethod.POST)
    public ResponseEntity<ListedEntry> createObject(@PathVariable("account_id") int account_id, @RequestParam(value = "name", required = false) String name, @RequestParam(value = "file", required = false) MultipartFile file, @PathVariable("directory") String parentDirectory) {
        checkRights(account_id);
        if (!(name == null ^ file == null)) {
            throw new BadRequest();
        }

        IFilesystemEntryResponse response;

        if (name == null) {
            response = createFile(account_id, file, parentDirectory);
        } else {
            response = createDirectory(account_id, name, parentDirectory);
        }

        response.setBaseUrl(getBaseUrl());
        HttpHeaders headers = new HttpHeaders();
        headers.set("Location", response.getUrl());

        return new ResponseEntity<ListedEntry>(new ListedEntry(response), headers, HttpStatus.CREATED);
    }

    /**
     * Method to rename and/or move the file in root folder.
     *
     * @param account_id
     * @param file
     * @param newName
     * @param newParentDirectory
     * @return
     */
    @RequestMapping(value = "/filesystem/{account_id:[0-9]+}/f{file:[a-zA-Z0-9]{32}}", method = RequestMethod.PUT)
    public ResponseEntity<ListedEntry> moveFile(@PathVariable("account_id") int account_id, @PathVariable("file") String file, @RequestParam(value = "name") String newName, @RequestParam(value = "parentDirectory", required = false) String newParentDirectory) {
        checkRights(account_id);
        return renameAndMoveFile(account_id, file, null, newName, newParentDirectory);
    }

    /**
     * Method to rename and/or move the file in a nested folder.
     *
     * @param account_id
     * @param file
     * @param directory
     * @param newName
     * @param newParentDirectory
     * @return
     */
    @RequestMapping(value = "/filesystem/{account_id:[0-9]+}/{directory:[0-9a-zA-Z]{32}}/f{file:[a-zA-Z0-9]{32}}", method = RequestMethod.PUT)
    public ResponseEntity<ListedEntry> moveFile(@PathVariable("account_id") int account_id, @PathVariable("file") String file, @PathVariable("directory") String directory, @RequestParam(value = "name") String newName, @RequestParam(value = "parentDirectory", required = false) String newParentDirectory) {
        checkRights(account_id);
        return renameAndMoveFile(account_id, file, directory, newName, newParentDirectory);
    }

    /**
     * Method to rename a directory
     *
     * @param account_id
     * @param directory
     * @param newName
     * @return
     */
    @RequestMapping(value = "/filesystem/{account_id:[0-9]+}/{directory:[0-9a-zA-Z]{32}}", method = RequestMethod.PUT)
    public ResponseEntity<ListedEntry> renameDirectory(@PathVariable("account_id") int account_id, @PathVariable("directory") String directory, @RequestParam(value = "name") String newName) {
        checkRights(account_id);

        DirectoryDescriptor dd = filesystem.getDirectory(account_id, directory);
        filesystem.renameDirectory(account_id, directory, newName);

        Directory d = new Directory(dd);
        d.setBaseUrl(getBaseUrl());
        
        ListedEntry response = new ListedEntry(d);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Location", response.getUrl());

        return new ResponseEntity<ListedEntry>(response, headers, HttpStatus.OK);
    }

    /**
     * Changes name and parent directory of a file.
     *
     * @param account_id
     * @param file
     * @param parentDirectory
     * @param newName
     * @param newParentDirectory
     * @return
     */
    private ResponseEntity<ListedEntry> renameAndMoveFile(int account_id, String file, String parentDirectory, String newName, String newParentDirectory) {
        if (!(newParentDirectory == null || newParentDirectory.length() == 32)) {
            throw new BadRequest();
        }

        if (newName.length() == 0) {
            throw new BadRequest();
        }

        FileDescriptor fd = filesystem.getFile(account_id, file, parentDirectory);
        if (fd.getParent() != newParentDirectory) {
            filesystem.moveFile(account_id, file, parentDirectory, newParentDirectory);
        }

        filesystem.renameFile(account_id, file, parentDirectory, newName);

        File f = new File(fd);
        f.setBaseUrl(getBaseUrl());

        ListedEntry response = new ListedEntry(f);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Location", response.getUrl());

        return new ResponseEntity<ListedEntry>(response, headers, HttpStatus.OK);
    }

    /**
     * Throws an exception when rights have been violated.
     *
     * @param account_id
     */
    private void checkRights(int account_id) {
        if (account_id != getLoggedUser().getID()) {
            throw new AccessDeniedException("You can only access your own object.");
        }
    }
}
