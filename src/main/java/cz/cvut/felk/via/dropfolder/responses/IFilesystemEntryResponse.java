package cz.cvut.felk.via.dropfolder.responses;

/**
 * Interface for filesystem entry responses.
 * @author Petr Kalivoda
 */
public interface IFilesystemEntryResponse extends IBaseUrlResponse {
    
    /**
     * Returns url of parent directory
     * @return 
     */
    public String getParentUrl();
    
    /**
     * Returns name of entry
     * @return 
     */
    public String getName();
    
    /**
     * Returns qualified name of entry
     * @return 
     */
    public String getObjectName();
    
    /**
     * "directory" or "file"
     * @return 
     */
    public String getType();
    
}
