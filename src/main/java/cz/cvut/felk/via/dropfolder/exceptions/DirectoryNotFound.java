package cz.cvut.felk.via.dropfolder.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception for missing directory
 *
 * @author Petr Kalivoda
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class DirectoryNotFound extends RuntimeException {

    public DirectoryNotFound(String directoryHash) {
        super(String.format("Directory '%s' does not exist.", directoryHash));
    }
}
