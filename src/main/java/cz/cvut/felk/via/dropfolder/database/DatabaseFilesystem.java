/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.felk.via.dropfolder.database;

import cz.cvut.felk.via.dropfolder.exceptions.Conflict;
import cz.cvut.felk.via.dropfolder.exceptions.DirectoryNotEmpty;
import cz.cvut.felk.via.dropfolder.exceptions.DirectoryNotFound;
import cz.cvut.felk.via.dropfolder.exceptions.FileNotFound;
import cz.cvut.felk.via.dropfolder.exceptions.ImmutableDirectory;
import cz.cvut.felk.via.dropfolder.filesystem.DirectoryDescriptor;
import cz.cvut.felk.via.dropfolder.filesystem.FileDescriptor;
import cz.cvut.felk.via.dropfolder.filesystem.FilesystemBase;
import cz.cvut.felk.via.dropfolder.filesystem.IEntryDescriptor;
import cz.cvut.felk.via.dropfolder.filesystem.IFileStorage;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.MessageDigestPasswordEncoder;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Revan
 */
public class DatabaseFilesystem extends FilesystemBase {

    final String CONFILICT = "In target directory exists object with same name already!";
    
    @Autowired
    public DatabaseFileStorage storage;
    
    
    public DatabaseFilesystem(IFileStorage storage, MessageDigestPasswordEncoder encoder) {
        super(storage, encoder);
    }

    @Override
    public boolean directoryExists(int account_id, String directoryHash) {
        return storage.exists(account_id, directoryHash);
    }

    @Override
    public DirectoryDescriptor createDirectory(int account_id, String directoryName, String parentDirectory) throws DirectoryNotFound, Conflict {
        if (parentDirectory != null && !storage.exists(account_id, parentDirectory)) throw new DirectoryNotFound(parentDirectory);
        if (storage.existsInFolder(account_id, directoryName, parentDirectory)) throw new Conflict(CONFILICT);
        
        DirectoryDescriptor directory = new DirectoryDescriptor(account_id, directoryName, getDirectoryName(account_id, directoryName), parentDirectory);
        storage.store(directory);
        
        return directory;
    }

    @Override
    public DirectoryDescriptor getDirectory(int account_id, String directoryHash) throws DirectoryNotFound {
        if (!storage.exists(account_id, directoryHash)) throw new DirectoryNotFound(directoryHash);
        
        return storage.getDirectory(account_id, directoryHash);
    }

    @Override
    public FileDescriptor createFile(int account_id, MultipartFile file, String parentDirectory) throws DirectoryNotFound, Conflict {
        if (!storage.exists(account_id, parentDirectory)) throw new DirectoryNotFound(parentDirectory);
        if (storage.existsInFolder(account_id, parentDirectory, file.getName())) throw new Conflict(CONFILICT);
        
        String fileHash = getFileName(account_id, parentDirectory);
        
        try {
            byte[] fileBytes = file.getBytes();
            FileDescriptor fileD = new FileDescriptor(account_id, file.getOriginalFilename(), fileHash, file.getContentType(), parentDirectory, fileBytes);
            storage.store(fileD);
            
            return fileD;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public boolean fileExists(int account_id, String fileHash) {
        return (storage.exists(account_id, fileHash));
    }

    @Override
    public FileDescriptor getFile(int account_id, String fileHash, String parentDirectory) throws FileNotFound {
        return storage.get(account_id, fileHash);
    }

    @Override
    public void deleteFile(int account_id, String fileHash, String parentDirectory) throws FileNotFound {
        if (!storage.exists(account_id, fileHash)) throw new FileNotFound(fileHash);
        storage.delete(account_id, fileHash);
    }

    @Override
    public void deleteDirectory(int account_id, String directoryHash, boolean recursive) throws DirectoryNotFound, DirectoryNotEmpty, ImmutableDirectory {
        if (!storage.exists(account_id, directoryHash)) throw new DirectoryNotFound(directoryHash);
        
        DirectoryDescriptor dir = storage.getDirectory(account_id, directoryHash);
        
        Collection<IEntryDescriptor> children = dir.getChildren();
        if (!recursive && !children.isEmpty()) throw new DirectoryNotEmpty(directoryHash);
        
        for(IEntryDescriptor entry : children) {
            if (entry instanceof FileDescriptor) {
                deleteFile(account_id, entry.getHash(), entry.getParent());
            } else {
                deleteDirectory(account_id, entry.getHash(), recursive);
            }
        }
        
        if (directoryHash != null) storage.delete(account_id, directoryHash);
    }

    @Override
    public void deleteAccount(int account_id) {
        deleteDirectory(account_id, null, true);
    }

    @Override
    public void renameFile(int account_id, String fileHash, String directoryHash, String newName) throws FileNotFound, Conflict {
        if (!storage.exists(account_id, fileHash)) throw new FileNotFound(fileHash);
        if (storage.existsInFolder(account_id, newName, directoryHash)) throw new Conflict(CONFILICT);
        
        storage.rename(account_id, fileHash, newName);
    }

    @Override
    public void moveFile(int account_id, String fileHash, String directoryHash, String newDirectoryHash) throws FileNotFound, DirectoryNotFound, Conflict {
        if (!storage.exists(account_id, fileHash)) throw new FileNotFound(fileHash);
        if (!storage.exists(account_id, newDirectoryHash)) throw new DirectoryNotFound(newDirectoryHash);
        
        FileDescriptor file = storage.get(account_id, fileHash);
        if (storage.existsInFolder(account_id, file.getName(), newDirectoryHash)) throw new Conflict(CONFILICT);
        
        storage.updateParent(account_id, fileHash, newDirectoryHash);
    }

    @Override
    public void renameDirectory(int account_id, String directoryHash, String newName) throws DirectoryNotFound, Conflict {
        if (!storage.exists(account_id, directoryHash) || directoryHash == null) throw new DirectoryNotFound(directoryHash);
        
        DirectoryDescriptor directory = getDirectory(account_id, directoryHash);
        if (storage.existsInFolder(account_id, newName, directory.getParent())) throw new Conflict(CONFILICT);

        storage.rename(account_id, directoryHash, newName);
    }
}
