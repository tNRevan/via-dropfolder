package cz.cvut.felk.via.dropfolder.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.UserDetailsManager;

/**
 * Interface for 
 * @author Petr Kalivoda
 */
public interface IIDSearchableUserDetailsManager extends UserDetailsManager {
    public UserDetails loadUserById(int ID) throws UserIDNotFoundException;
    
    public User createUser(String userName, String password);
}
