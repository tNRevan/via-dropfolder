package cz.cvut.felk.via.dropfolder.filesystem;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

/**
 * Descriptor for a directory
 * @author Petr Kalivoda
 */
public class DirectoryDescriptor implements IEntryDescriptor {

    private final int account_id;
    private String name;
    private final String hash;
    private String parent;
    private final boolean is_root;
    
    private final HashMap<String, IEntryDescriptor> children = new HashMap<String, IEntryDescriptor>();

    public DirectoryDescriptor(int account_id, String name, String hash, String parent, boolean is_root) {
        this.account_id = account_id;
        this.name = name;
        this.hash = hash;
        this.parent = parent;
        this.is_root = is_root;
    }
    
    public DirectoryDescriptor(int account_id, String name, String hash, String parent) {
        this(account_id, name, hash, parent, false);
    }
    
    public void setName(String newName) {
        if(newName == null || newName.length() == 0) {
            //nothing to do here!
            return;
        }
        
        name = newName;
    }
    
    public void setParent(String newParent) {
        parent = newParent;
    }
    
    /**
     * Adds a child entry
     * @param ed 
     */
    public void addChild(IEntryDescriptor ed) {
        children.put(ed.getHash(), ed);
    }
    
    /**
     * Removes a child entry.
     * 
     * @param key 
     */
    public void removeChild(String key) {
        children.remove(key);
    }
    
    public Collection<IEntryDescriptor> getChildren() {
        return Collections.unmodifiableCollection(children.values());
    }
     
    @Override
    public int getAccountId() {
        return account_id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getHash() {
        return hash;
    }

    @Override
    public String getParent() {
        return parent;
    }

    @Override
    public EntryType getType() {
        return EntryType.DIRECTORY;
    }
    
    public boolean isRoot() {
        return is_root;
    }
}
