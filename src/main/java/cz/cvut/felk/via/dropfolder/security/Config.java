package cz.cvut.felk.via.dropfolder.security;

import cz.cvut.felk.via.dropfolder.database.DatabaseFileStorage;
import cz.cvut.felk.via.dropfolder.database.DatabaseFilesystem;
import cz.cvut.felk.via.dropfolder.database.DatabaseUsers;
import cz.cvut.felk.via.dropfolder.filesystem.IFileStorage;
import cz.cvut.felk.via.dropfolder.filesystem.IFilesystem;
import javax.servlet.MultipartConfigElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.encoding.MessageDigestPasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;


/**
 *
 * @author Petr Kalivoda
 */
@Configuration
@EnableWebSecurity
public class Config extends WebSecurityConfigurerAdapter {

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(this.getUserDetailsManager());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/account/").permitAll()
                .antMatchers("/**").authenticated()
                .and().httpBasic()
                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().csrf().disable(); //no CSRF protection in REST (duh!)

    }

    @Bean
    public IIDSearchableUserDetailsManager getUserDetailsManager() {
        return new DatabaseUsers();
    }

    @Bean
    public IFilesystem getFilesystem() {
        return new DatabaseFilesystem(getFileStorage(), new MessageDigestPasswordEncoder("md5"));
    }

    @Bean
    public IFileStorage getFileStorage() {
        return new DatabaseFileStorage();
    }

    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setMaxFileSize("512MB"); 
        factory.setMaxRequestSize("512MB");
        return factory.createMultipartConfig();
    }
}
