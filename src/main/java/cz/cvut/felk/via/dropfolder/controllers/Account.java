package cz.cvut.felk.via.dropfolder.controllers;

import cz.cvut.felk.via.dropfolder.database.DatabaseConnection;
import cz.cvut.felk.via.dropfolder.exceptions.Conflict;
import cz.cvut.felk.via.dropfolder.responses.UserDetails;
import cz.cvut.felk.via.dropfolder.security.IIDSearchableUserDetailsManager;
import cz.cvut.felk.via.dropfolder.security.InMemoryUser;
import cz.cvut.felk.via.dropfolder.security.User;
import cz.cvut.felk.via.dropfolder.filesystem.IFilesystem;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for account-based operations
 * @author Petr Kalivoda
 */
@RestController
public class Account extends Base{

    private final IIDSearchableUserDetailsManager userDetailsManager;
    
    private final IFilesystem filesystem;

    
    @Autowired
    public Account(IIDSearchableUserDetailsManager userDetailsManager, IFilesystem filesystem) {
        this.userDetailsManager = userDetailsManager;
        this.filesystem = filesystem;
    }

    /**
     * Serves just to validate login.
     * Returns 200 OK and user details or fail.
     *
     * @return
     */
    @RequestMapping(value = "/account/login", method = RequestMethod.GET)
    public UserDetails login() {
        UserDetails ud = new UserDetails(getLoggedUser());
        ud.setBaseUrl(getBaseUrl());
        return ud;
    }

    /**
     * Registration method
     * @param username
     * @param password
     * @throws Conflict
     * @return 
     */
    @RequestMapping(value = "/account/", method = RequestMethod.POST)
    public ResponseEntity<UserDetails> register(@RequestParam("username") String username, @RequestParam("password") String password) throws Conflict {
        if(userDetailsManager.userExists(username)) {
            throw new Conflict(String.format("User '%s' already exists.", username));
        }
        
        User user = userDetailsManager.createUser(username, password);
        UserDetails ud = new UserDetails(user);
        ud.setBaseUrl(getBaseUrl());
        
        HttpHeaders headers = new HttpHeaders();
        headers.set("Location", ud.getUrl());
        return  new ResponseEntity<UserDetails>(ud, headers, HttpStatus.CREATED);
    }

    /**
     * Returns user details.
     * @param account_id
     * @todo admin users?
     * @return 
     */
    @RequestMapping(value = "/account/{account_id}", method = RequestMethod.GET)
    public UserDetails getAccount(@PathVariable("account_id") int account_id) {
        User u = getLoggedUser();
        if(u.getID() != account_id) {
            throw new AccessDeniedException("You can only view your own account.");
        }
        
        UserDetails ud = new UserDetails(u);
        ud.setBaseUrl(getBaseUrl());
        return ud;
    }
    
    /**
     * Changes user's password
     * @param account_id
     * @param new_password
     * @return 
     */
    @RequestMapping(value = "/account/{account_id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> changePassword(@PathVariable("account_id") int account_id, @RequestParam("new_password") String new_password) {
        User u = getLoggedUser();
        if(u.getID() != account_id) {
            throw new AccessDeniedException("You can only change your own password.");
        }
        
        userDetailsManager.changePassword(u.getPassword(), new_password);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }
    
    /**
     * Deletes and user.
     * @todo admin users?
     * @todo delete all folders
     * @param account_id
     * @return 
     */
    @RequestMapping(value = "/account/{account_id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteUser(@PathVariable("account_id") int account_id) {
        User u = getLoggedUser();
        if(u.getID() != account_id) {
            throw new AccessDeniedException("You can only delete yourself.");
        }
        
        userDetailsManager.deleteUser(u.getUsername());
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }
    
    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public ResponseEntity<Void> test() {
        DatabaseConnection db = new DatabaseConnection();
        //db.initDB();
        
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }
}
