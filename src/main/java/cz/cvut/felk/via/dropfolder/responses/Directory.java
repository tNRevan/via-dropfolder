package cz.cvut.felk.via.dropfolder.responses;

import cz.cvut.felk.via.dropfolder.filesystem.DirectoryDescriptor;
import cz.cvut.felk.via.dropfolder.filesystem.FileDescriptor;
import cz.cvut.felk.via.dropfolder.filesystem.IEntryDescriptor;
import java.util.LinkedList;
import java.util.List;

/**
 * Response for a directory.
 *
 * @author Petr Kalivoda
 */
public class Directory implements IFilesystemEntryResponse {

    private final DirectoryDescriptor descriptor;
    
    private String baseURL = "";

    public Directory(DirectoryDescriptor dd) {
        descriptor = dd;
    }
    
    @Override
    public void setBaseUrl(String baseURL) {
        this.baseURL = baseURL;
    }

    @Override
    public String getUrl() {
        if (descriptor.isRoot()) {
            return String.format("%s/filesystem/%d", baseURL, descriptor.getAccountId());
        }
        return String.format("%s/filesystem/%d/%s", baseURL, descriptor.getAccountId(), descriptor.getHash());
    }

    @Override
    public String getParentUrl() {
        if (descriptor.isRoot()) {
            return null;
        }
        
        if(descriptor.getParent() == null) {
            return String.format("%s/filesystem/%d", baseURL, descriptor.getAccountId());
        }

        return String.format("%s/filesystem/%d/%s", baseURL, descriptor.getAccountId(), descriptor.getParent());
    }

    @Override
    public String getName() {
        return descriptor.getName();
    }

    @Override
    public String getObjectName() {
        return descriptor.getHash();
    }

    @Override
    public String getType() {
        return "directory";
    }

    /**
     * Returns contents of the directory.
     *
     * @return
     */
    public List<ListedEntry> getData() {
        List<ListedEntry> data = new LinkedList<ListedEntry>();
        for (IEntryDescriptor child : descriptor.getChildren()) {

            if (child.getType() == IEntryDescriptor.EntryType.DIRECTORY) {
                Directory d = new Directory((DirectoryDescriptor) child);
                d.setBaseUrl(baseURL);
                
                data.add(new ListedEntry(d));
            } else if (child.getType() == IEntryDescriptor.EntryType.FILE) {
                File f = new File((FileDescriptor) child);
                f.setBaseUrl(baseURL);
                
                data.add(new ListedEntry(f));
            }
        }

        return data;
    }
}
