package cz.cvut.felk.via.dropfolder.filesystem;

import cz.cvut.felk.via.dropfolder.exceptions.FileNotFound;
import java.util.HashMap;

/**
 * IFileStorage implementation for storing files in memory.
 * 
 * @author Petr Kalivoda
 */
public class InMemoryFileStorage implements IFileStorage {
    
    private final HashMap<String, FileDescriptor> files = new HashMap<String, FileDescriptor>();

    @Override
    public FileDescriptor get(int account_id, String key) throws FileNotFound {
        if(!files.containsKey(getKey(account_id, key))) {
            throw new FileNotFound(key);
        }
        
        return files.get(getKey(account_id, key));
    }
    
    @Override
    public void store(FileDescriptor file) {
        files.put(getKey(file.getAccountId(), file.getHash()), file);
    }
    
    private String getKey(int account_id, String key) {
        return String.format("%d|%s", account_id, key);
    }

    @Override
    public boolean exists(int account_id, String key) {
        return files.containsKey(getKey(account_id, key));
    }

    @Override
    public void delete(int account_id, String key) throws FileNotFound {
        if(!exists(account_id, key)) {
            throw new FileNotFound(key);
        }
        
        files.remove(getKey(account_id, key));
    }
    
}
