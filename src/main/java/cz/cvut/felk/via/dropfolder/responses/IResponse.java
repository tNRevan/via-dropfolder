package cz.cvut.felk.via.dropfolder.responses;

/**
 * Generic response interface
 * @author Petr Kalivoda
 */
public interface IResponse {
    
    /**
     * Returns url of the resource.
     * @return 
     */
    public String getUrl();
}
