package cz.cvut.felk.via.dropfolder.filesystem;

/**
 * Interface for an entry descriptor.
 * @author Petr Kalivoda
 */
public interface IEntryDescriptor {
    
    enum EntryType {
        FILE, DIRECTORY;
    }
    
    public String getHash();
    
    public EntryType getType();
    
    public int getAccountId();
    
    public String getParent();
    
    public String getName();
}
