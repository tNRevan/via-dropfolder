package cz.cvut.felk.via.dropfolder.database;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.apache.commons.io.IOUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author Revan
 * @author Petr Kalivoda
 */
@Configuration
public class DatabaseConnection {

    private static final String JDBC_DRIVER;
    private static final Integer PORT;
    private static final String DATABASE;
    private static final String DB_URL;
    private static final String DB_USER;
    private static final String DB_PASS;

    /**
     * Initializes db connection info from property file
     * classpath:persistence.properties (should be present in resources)
     */
    static {

        try {
            Properties p = new Properties();
            p.load(DatabaseConnection.class.getResourceAsStream("persistence.properties"));
            JDBC_DRIVER = p.getProperty("persistence.driver");
            PORT = Integer.parseInt(p.getProperty("persistence.port"));
            DATABASE = p.getProperty("persistence.database");
            DB_URL = p.getProperty("persistence.hostname");
            DB_USER = p.getProperty("persistence.username");
            DB_PASS = p.getProperty("persistence.password");

        } catch (IOException e) {
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @Bean(destroyMethod = "close")
    public DataSource datasource() throws SQLException {
        initSchema();
        
        org.apache.tomcat.jdbc.pool.DataSource ds = new org.apache.tomcat.jdbc.pool.DataSource();
        ds.setDriverClassName(JDBC_DRIVER);
        ds.setUrl(String.format("%s:%d/%s", DB_URL, PORT, DATABASE));
        ds.setUsername(DB_USER);
        ds.setPassword(DB_PASS);
        ds.setInitialSize(5);
        ds.setMaxActive(50);
        ds.setMaxIdle(5);
        ds.setMinIdle(2);
        ds.setLogValidationErrors(true);
        ds.setRemoveAbandoned(true);
        ds.setRemoveAbandonedTimeout(60); //60 seconds and the pool is kill
        ds.setValidationQuery("SELECT * FROM users");
        return ds;
    }

    /**
     * Returns init sql statement
     *
     * @return
     */
    private static String[] getInitSql() {
        InputStream is = DatabaseConnection.class.getResourceAsStream("schema.sql");
        StringWriter sw = new StringWriter();
        try {
            IOUtils.copy(is, sw, "UTF-8");
        } catch (IOException e) {
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, "Loading SQL schema from file failed.", e);
            throw new RuntimeException(e);
        }

        return String.format(sw.toString(), DATABASE, DATABASE, DATABASE).split(";"); //magic!
    }

    private void initSchema() throws SQLException {
        Connection c = DriverManager.getConnection(String.format("%s:%d", DB_URL, PORT), DB_USER, DB_PASS);
        Statement stmt = c.createStatement();

        for (String s : getInitSql()) {
            //all of this is just because jdbc can't for the love of god accept ; separated statements :X
            s = s.trim();
            if (s.length() > 0) {
                stmt.addBatch(s);
            }
        }
        //also, f**k all this!
        stmt.executeBatch();
        c.close();
    }
}
