/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.cvut.felk.via.dropfolder.security;

import java.util.Collection;
import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author Petr Kalivoda
 */
public class InMemoryUser extends User {
    
    private static int ID = 1;

    public InMemoryUser(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(ID++, username, password, authorities);
    }
    
    @Override
    public void eraseCredentials() {
        //and I've been wondering why can I login only once per server run, PFF...
    }
    
}
