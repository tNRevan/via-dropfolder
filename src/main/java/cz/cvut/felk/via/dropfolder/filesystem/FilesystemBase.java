package cz.cvut.felk.via.dropfolder.filesystem;

import org.springframework.security.authentication.encoding.MessageDigestPasswordEncoder;

/**
 * Base for a filesystem implementation
 *
 * @author Petr Kalivoda
 */
abstract public class FilesystemBase implements IFilesystem {

    protected final IFileStorage fileStorage;
    protected final MessageDigestPasswordEncoder encoder;

    public FilesystemBase(IFileStorage storage, MessageDigestPasswordEncoder encoder) {
        this.fileStorage = storage;
        this.encoder = encoder;
    }

    @Override
    public final String getDirectoryName(int account_id, String realName) {
        return encoder.encodePassword(String.format("%d:%d:%s", System.currentTimeMillis(), account_id, realName), Math.random());
    }
    
    @Override
    public final String getFileName(int account_id, String realName) {
        return getDirectoryName(account_id, realName);
    }

}
