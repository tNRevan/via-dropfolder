package cz.cvut.felk.via.dropfolder.controllers;

import cz.cvut.felk.via.dropfolder.security.User;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * Base class for all controllers (providing user service)
 *
 * @author Petr Kalivoda
 */
abstract class Base {

    public String getBaseUrl() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        return String.format("%s://%s%s", request.getScheme(), request.getServerName(), request.getServerPort() == 80 ? "" : (":" + request.getServerPort()));
    }

    /**
     * Returns logged user.
     *
     * @return
     */
    public User getLoggedUser() {
        return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
