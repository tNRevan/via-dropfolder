/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.felk.via.dropfolder.database;

import cz.cvut.felk.via.dropfolder.exceptions.Conflict;
import cz.cvut.felk.via.dropfolder.exceptions.DirectoryNotFound;
import cz.cvut.felk.via.dropfolder.exceptions.FileNotFound;
import cz.cvut.felk.via.dropfolder.filesystem.DirectoryDescriptor;
import cz.cvut.felk.via.dropfolder.filesystem.FileDescriptor;
import cz.cvut.felk.via.dropfolder.filesystem.IEntryDescriptor;
import cz.cvut.felk.via.dropfolder.filesystem.IFileStorage;
import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.sql.DataSource;

/**
 *
 * @author Revan
 */
public class DatabaseFileStorage implements IFileStorage {

    public static final String DIRECTORY_TYPE = "DIRECTORY";
    final String TABLE = "files";

    @Resource
    private DataSource datasource;

    /// FILES ///
    @Override
    public FileDescriptor get(int account_id, String hash) throws FileNotFound {
        FileEntry file = getFileInternal(account_id, hash);

        if (file == null) {
            throw new FileNotFound(hash);
        }
        if (!file.isIsFile()) {
            throw new Conflict("It's not a file!");
        }

        return (FileDescriptor) file.entry();
    }

    public DirectoryDescriptor getDirectory(int account_id, String hash) throws DirectoryNotFound {
        DirectoryDescriptor dd;

        if (hash == null) {
            dd = new DirectoryDescriptor(account_id, null, null, null, true);
        } else {
            FileEntry file = getFileInternal(account_id, hash);

            if (file == null) {
                throw new DirectoryNotFound(hash);
            }
            if (file.isIsFile()) {
                throw new Conflict("It's not a folder!");
            }

            dd = (DirectoryDescriptor) file.entry();
        }

        List<IEntryDescriptor> entries = entriesWithParent(account_id, hash);
        for (IEntryDescriptor entry : entries) {
            dd.addChild(entry);
        }

        return dd;
    }

    private List<IEntryDescriptor> entriesWithParent(int account_id, String parentHash) {
        List<IEntryDescriptor> entries = new ArrayList<IEntryDescriptor>();

        String select = "SELECT id FROM " + TABLE + " WHERE owner_id=? AND parent_id <=> ?;";

        try {
            Connection c = datasource.getConnection();
            PreparedStatement stmt = c.prepareStatement(select);
            stmt.setInt(1, account_id);
            stmt.setString(2, parentHash);

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                String id = rs.getString(1);

                FileEntry file = getFileInternal(account_id, id);
                entries.add(file.entry());
            }

            c.close();

        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

        return entries;
    }

    private FileEntry getFileInternal(int account_id, String key) {
        String select = "SELECT id, parent_id, data, owner_id, type, name FROM " + TABLE
                + " WHERE owner_id=? AND id=?;";

        try {
            Connection c = datasource.getConnection();
            PreparedStatement stmt = c.prepareStatement(select);
            stmt.setInt(1, account_id);
            stmt.setString(2, key);

            ResultSet rs = stmt.executeQuery();
            if (!rs.next()) {
                return null;
            }

            FileEntry fe = new FileEntry();
            fe.setAccountId(account_id).setContentType(rs.getString("type")).setData(rs.getBytes("data"))
                    .setHash(key).setName(rs.getString("name")).setParent(rs.getString("parent_id"));
            fe.setIsFile(!DIRECTORY_TYPE.equals(rs.getString("type")));
            c.close();
            return fe;

        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void store(FileDescriptor file) {
        store(new FileEntry(file));
    }

    public void store(DirectoryDescriptor file) {
        store(new FileEntry(file));
    }

    private void store(FileEntry file) {
        if (exists(file.getAccountId(), file.getHash())) {
            String update = "UPDATE " + TABLE + " SET parent_id=?, data=?, name=?, type=? "
                    + " WHERE id=? AND owner_id=?;";
            try {
                Connection c = datasource.getConnection();
                PreparedStatement stmt = c.prepareStatement(update);

                stmt.setString(1, file.getParent());
                stmt.setBinaryStream(2, new ByteArrayInputStream(file.getData()));
                stmt.setString(3, file.getName());
                stmt.setString(4, file.getContentType());
                stmt.setString(5, file.getHash());
                stmt.setInt(6, file.getAccountId());

                stmt.executeUpdate();
                c.close();

            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        } else {
            String insert = "INSERT INTO " + TABLE + " (id, parent_id, data, owner_id, type, name) "
                    + " VALUES (?,?,?,?,?,?);";
            try {
                Connection c = datasource.getConnection();
                PreparedStatement stmt = c.prepareStatement(insert);

                stmt.setString(1, file.getHash());
                stmt.setString(2, file.getParent());
                stmt.setBytes(3, file.getData());
                stmt.setInt(4, file.getAccountId());
                stmt.setString(5, file.getContentType());
                stmt.setString(6, file.getName());

                stmt.executeUpdate();
                c.close();

            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    /// Universal
    @Override
    public boolean exists(int account_id, String hash) {
        if (hash == null) {
            return true; // Root Directory
        }
        String select = "SELECT id FROM " + TABLE
                + " WHERE owner_id=? AND id=?;";

        try {
            Connection c = datasource.getConnection();
            PreparedStatement stmt = c.prepareStatement(select);
            stmt.setInt(1, account_id);
            stmt.setString(2, hash);

            ResultSet rs = stmt.executeQuery();
            c.close();
            return rs.next();

        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    public boolean existsInFolder(int account_id, String fileName, String parentFolder) {
        String select = "SELECT id FROM " + TABLE + " WHERE owner_id=? AND name=? AND parent_id <=> ?;";

        try {
            Connection c = datasource.getConnection();
            PreparedStatement stmt = c.prepareStatement(select);
            stmt.setInt(1, account_id);
            stmt.setString(2, fileName);
            stmt.setString(3, parentFolder);

            ResultSet rs = stmt.executeQuery();
            c.close();
            return rs.next();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public void rename(int account_id, String fileHash, String newName) {
        String update = "UPDATE " + TABLE + " SET name=? "
                + " WHERE id=? AND owner_id=?;";

        try {
            Connection c = datasource.getConnection();
            PreparedStatement stmt = c.prepareStatement(update);
            stmt.setString(1, newName);
            stmt.setString(2, fileHash);
            stmt.setInt(3, account_id);

            stmt.executeUpdate();
            c.close();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public void updateParent(int account_id, String fileHash, String parentHash) {
        String update = "UPDATE " + TABLE + " SET parent_id=? "
                + " WHERE id=? AND owner_id=?;";

        try {
            Connection c = datasource.getConnection();
            PreparedStatement stmt = c.prepareStatement(update);
            stmt.setString(1, parentHash);
            stmt.setString(2, fileHash);
            stmt.setInt(3, account_id);

            stmt.executeUpdate();
            c.close();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void delete(int account_id, String key) throws FileNotFound {
        if (key == null) {
            throw new FileNotFound(null);
        }
        if (!exists(account_id, key)) {
            throw new FileNotFound(key);
        }

        String delete = "DELETE FROM " + TABLE + " WHERE owner_id=? AND id=?;";

        try {
            Connection c = datasource.getConnection();
            PreparedStatement stmt = c.prepareStatement(delete);
            stmt.setInt(1, account_id);
            stmt.setString(2, key);

            stmt.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    class FileEntry {

        String hash;
        String name;
        String parent;
        String contentType;
        int accountId;
        byte[] data;
        boolean isFile;

        FileEntry() {
        }

        ;
        
        FileEntry(FileDescriptor fd) {
            hash = fd.getHash();
            name = fd.getName();
            parent = fd.getParent();
            contentType = fd.getContentType();
            accountId = fd.getAccountId();
            data = fd.getData();
            isFile = true;
        }

        FileEntry(DirectoryDescriptor dd) {
            hash = dd.getHash();
            name = dd.getName();
            parent = dd.getParent();
            contentType = DIRECTORY_TYPE;
            accountId = dd.getAccountId();
            data = null;
            isFile = false;
        }

        IEntryDescriptor entry() {
            if (isFile) {
                return new FileDescriptor(getAccountId(), getName(), getHash(), getContentType(), getParent(), getData());
            } else {
                return new DirectoryDescriptor(accountId, name, hash, parent, false);
            }
        }

        public String getHash() {
            return hash;
        }

        public String getName() {
            return name;
        }

        public String getParent() {
            return parent;
        }

        public String getContentType() {
            return contentType;
        }

        public int getAccountId() {
            return accountId;
        }

        public byte[] getData() {
            return data;
        }

        public boolean isIsFile() {
            return isFile;
        }

        public FileEntry setHash(String hash) {
            this.hash = hash;
            return this;
        }

        public FileEntry setName(String name) {
            this.name = name;
            return this;
        }

        public FileEntry setParent(String parent) {
            this.parent = parent;
            return this;
        }

        public FileEntry setContentType(String contentType) {
            this.contentType = contentType;
            return this;
        }

        public FileEntry setAccountId(int accountId) {
            this.accountId = accountId;
            return this;
        }

        public FileEntry setData(byte[] data) {
            this.data = data;
            return this;
        }

        public FileEntry setIsFile(boolean isFile) {
            this.isFile = isFile;
            return this;
        }
    }
}
