package cz.cvut.felk.via.dropfolder.responses;

import cz.cvut.felk.via.dropfolder.filesystem.FileDescriptor;
import org.springframework.security.crypto.codec.Base64;

/**
 * Response for a file object
 * @author Petr Kalivoda
 */
public class File implements IFilesystemEntryResponse {
    
    private final FileDescriptor descriptor;
    
    private String baseUrl = "";
    
    public File(FileDescriptor fd) {
        descriptor = fd;
    }
    
    @Override
    public void setBaseUrl(String baseURL) {
        this.baseUrl = baseURL;
    }

    @Override
    public String getParentUrl() {
        if(descriptor.getParent() == null) {
            return String.format("%s/filesystem/%d", baseUrl, descriptor.getAccountId());
        }
        
        return String.format("%s/filesystem/%d/%s", baseUrl, descriptor.getAccountId(), descriptor.getParent());
    }

    @Override
    public String getName() {
        return descriptor.getName();
    }

    @Override
    public String getObjectName() {
        return descriptor.getHash();
    }

    @Override
    public String getType() {
        return "file";
    }

    @Override
    public String getUrl() {
        if(descriptor.getParent() == null) {
            return String.format("%s/filesystem/%d/f%s", baseUrl, descriptor.getAccountId(), descriptor.getHash());
        }
        return String.format("%s/filesystem/%d/%s/f%s", baseUrl, descriptor.getAccountId(), descriptor.getParent(), descriptor.getHash());
    }
    
    public String getData() {
        byte[] encoded = Base64.encode(descriptor.getData());
        return new String(encoded);
    }
    
    public String getContentType() {
        return descriptor.getContentType();
    }
    
}
