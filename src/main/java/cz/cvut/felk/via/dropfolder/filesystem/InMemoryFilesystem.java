package cz.cvut.felk.via.dropfolder.filesystem;

import cz.cvut.felk.via.dropfolder.exceptions.Conflict;
import cz.cvut.felk.via.dropfolder.exceptions.DirectoryNotEmpty;
import cz.cvut.felk.via.dropfolder.exceptions.DirectoryNotFound;
import cz.cvut.felk.via.dropfolder.exceptions.FileNotFound;
import cz.cvut.felk.via.dropfolder.exceptions.ImmutableDirectory;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.springframework.security.authentication.encoding.MessageDigestPasswordEncoder;
import org.springframework.web.multipart.MultipartFile;

/**
 * In-memory filesystem model.
 *
 * @author Petr Kalivoda
 */
public class InMemoryFilesystem extends FilesystemBase {

    /**
     * Default name for home directories.
     */
    private static final String HOME_DIRECTORY_KEY = "$HOME$";

    protected Map<Integer, HashMap<String, DirectoryDescriptor>> directories = new HashMap<Integer, HashMap<String, DirectoryDescriptor>>();

    public InMemoryFilesystem(IFileStorage storage, MessageDigestPasswordEncoder encoder) {
        super(storage, encoder);
    }

    @Override
    public boolean directoryExists(int account_id, String directoryHash) {
        ensureAccountExists(account_id);
        return directories.get(account_id).containsKey(directoryHash);
    }

    /**
     * Ensures that the account exists at all.
     *
     * @param account_id
     */
    private void ensureAccountExists(int account_id) {
        if (!directories.containsKey(account_id)) {
            HashMap<String, DirectoryDescriptor> account = new HashMap<String, DirectoryDescriptor>();
            account.put(HOME_DIRECTORY_KEY, new DirectoryDescriptor(account_id, "Home", null, null, true));
            directories.put(account_id, account);
        }
    }

    @Override
    public DirectoryDescriptor createDirectory(int account_id, String directoryName, String parentDirectory) throws Conflict, DirectoryNotFound {
        ensureAccountExists(account_id);
        if (parentDirectory == null) {
            parentDirectory = HOME_DIRECTORY_KEY;
        }

        //1. compute directory hash & create directory
        String directoryHash = getDirectoryName(account_id, directoryName);

        //2. check consistency
        if (!directoryExists(account_id, parentDirectory)) {
            throw new DirectoryNotFound(parentDirectory);
        }

        if (directoryExists(account_id, directoryHash)) {
            throw new Conflict("Directory already exists.");
        }

        //3. fetch account
        HashMap<String, DirectoryDescriptor> account = directories.get(account_id);

        //4. create the directory itself
        DirectoryDescriptor directory = new DirectoryDescriptor(account_id, directoryName, directoryHash, parentDirectory.equals(HOME_DIRECTORY_KEY) ? null : parentDirectory);
        account.put(directoryHash, directory);

        //5. register it in the parent directory        
        account.get(parentDirectory).addChild(directory);

        return directory;
    }

    @Override
    public DirectoryDescriptor getDirectory(int account_id, String directoryHash) throws DirectoryNotFound {
        ensureAccountExists(account_id);

        if (directoryHash == null) {
            directoryHash = HOME_DIRECTORY_KEY;
        }

        if (!directoryExists(account_id, directoryHash)) {
            throw new DirectoryNotFound(directoryHash);
        }

        return directories.get(account_id).get(directoryHash);
    }

    @Override
    public FileDescriptor createFile(int account_id, MultipartFile file, String parentDirectory) throws DirectoryNotFound, Conflict {
        ensureAccountExists(account_id);

        if (parentDirectory == null) {
            parentDirectory = HOME_DIRECTORY_KEY;
        }

        if (!directoryExists(account_id, parentDirectory)) {
            throw new DirectoryNotFound(parentDirectory);
        }

        //1. assign filename
        String hash = getFileName(account_id, file.getOriginalFilename());

        //2. fetch data
        byte[] data = new byte[(int) file.getSize()];

        try {
            System.arraycopy(file.getBytes(), 0, data, 0, (int) file.getSize());
        } catch (IOException e) {
            //blah blah blah, this should not happen and create a 500 error if it does.
            throw new RuntimeException(e);
        }

        //3. create file descriptor
        FileDescriptor fd = new FileDescriptor(account_id, file.getOriginalFilename(), hash, file.getContentType(), parentDirectory.equals(HOME_DIRECTORY_KEY) ? null : parentDirectory, data);

        //4. save file
        fileStorage.store(fd);

        //5. put file into directory
        DirectoryDescriptor dd = getDirectory(account_id, parentDirectory.equals(HOME_DIRECTORY_KEY) ? null : parentDirectory);
        dd.addChild(fd);

        return fd;
    }

    @Override
    public boolean fileExists(int account_id, String fileHash) {
        ensureAccountExists(account_id);
        return fileStorage.exists(account_id, fileHash);
    }

    @Override
    public FileDescriptor getFile(int account_id, String fileHash, String parentDirectory) throws FileNotFound {
        ensureAccountExists(account_id);
        return fileStorage.get(account_id, fileHash);
    }

    @Override
    public void deleteFile(int account_id, String fileHash, String parentDirectory) throws FileNotFound {
        ensureAccountExists(account_id);
        if (parentDirectory == null) {
            parentDirectory = HOME_DIRECTORY_KEY;
        }

        DirectoryDescriptor dd = getDirectory(account_id, parentDirectory);
        dd.removeChild(fileHash);

        fileStorage.delete(account_id, fileHash);
    }

    @Override
    public void deleteDirectory(int account_id, String directoryHash, boolean recursive) throws DirectoryNotFound, DirectoryNotEmpty, ImmutableDirectory {
        ensureAccountExists(account_id);
        if (directoryHash == null) {
            throw new ImmutableDirectory();
        }

        DirectoryDescriptor dd = getDirectory(account_id, directoryHash);
        if (!recursive && !dd.getChildren().isEmpty()) {
            //attemt to non-recursively delete a non-empty directory
            throw new DirectoryNotEmpty(directoryHash);
        }

        if (recursive) {
            //(0). delete all children
            //this is where the fun begins
            //also, this is highly non-atomic
            for (IEntryDescriptor ed : dd.getChildren()) {
                if (ed instanceof FileDescriptor) {
                    deleteFile(account_id, ed.getHash(), directoryHash);
                } else if (ed instanceof DirectoryDescriptor) {
                    deleteDirectory(account_id, ed.getHash(), true);
                }
            }
        }
        
        //1. unregister from parent directory
        DirectoryDescriptor parent = getDirectory(account_id, dd.getParent());
        parent.removeChild(directoryHash);

        //2. unregister from global storage
        directories.get(account_id).remove(directoryHash);
    }

    @Override
    public void deleteAccount(int account_id) {
        ensureAccountExists(account_id);
        DirectoryDescriptor home = getDirectory(account_id, HOME_DIRECTORY_KEY);
        
        //1. delete all data
        for (IEntryDescriptor ed : home.getChildren()) {
                if (ed instanceof FileDescriptor) {
                    deleteFile(account_id, ed.getHash(), home.getHash());
                } else if (ed instanceof DirectoryDescriptor) {
                    deleteDirectory(account_id, ed.getHash(), true);
                }
            }
        
        //2. remove home folder
        directories.get(account_id).remove(HOME_DIRECTORY_KEY);
        
        //3. remove account itself
        directories.remove(account_id);
    }

    @Override
    public void renameFile(int account_id, String fileHash, String directoryHash, String newName) throws FileNotFound {
        FileDescriptor fd = getFile(account_id, fileHash, directoryHash);
        fd.setName(newName);
    }

    @Override
    public void moveFile(int account_id, String fileHash, String directoryHash, String newDirectoryHash) throws FileNotFound {
        FileDescriptor fd = getFile(account_id, fileHash, directoryHash);
        DirectoryDescriptor oldDirectory = getDirectory(account_id, directoryHash == null ? HOME_DIRECTORY_KEY : directoryHash);
        DirectoryDescriptor newDirectory = getDirectory(account_id, newDirectoryHash == null ? HOME_DIRECTORY_KEY : newDirectoryHash);
        
        //1. change parent in filedesc
        fd.setParent(newDirectoryHash);
        
        //2. swap file from old to new
        oldDirectory.removeChild(fileHash);
        newDirectory.addChild(fd);
    }

    @Override
    public void renameDirectory(int account_id, String directoryHash, String newName) throws DirectoryNotFound {
        DirectoryDescriptor dd = getDirectory(account_id, directoryHash);
        dd.setName(newName);
    }
}
