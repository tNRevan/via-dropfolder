package cz.cvut.felk.via.dropfolder.filesystem;

import cz.cvut.felk.via.dropfolder.exceptions.FileNotFound;

/**
 * Datasource for files
 *
 * @author Petr Kalivoda
 */
public interface IFileStorage {

    /**
     * Looks up a file for a certain account
     *
     * @param account_id
     * @param key
     * @return
     * @throws FileNotFound
     */
    public FileDescriptor get(int account_id, String key) throws FileNotFound;
    
    /**
     * Checks if file exists
     * @param account_id
     * @param key
     * @return 
     */
    public boolean exists(int account_id, String key);

    /**
     * Stores a file in storage.
     *
     * @param file
     */
    public void store(FileDescriptor file);
    
    /**
     * Deletes a file from the storage.
     * 
     * @param account_id
     * @param key
     * @throws FileNotFound 
     */
    public void delete(int account_id, String key) throws FileNotFound;
}
