package cz.cvut.felk.via.dropfolder.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception for not-found files.
 * @author Petr Kalivoda
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class FileNotFound extends RuntimeException {

    public FileNotFound(String filename) {
        super(String.format("File '%s' was not found.", filename));
    }
}
