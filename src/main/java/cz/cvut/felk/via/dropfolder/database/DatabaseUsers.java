/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.felk.via.dropfolder.database;

import cz.cvut.felk.via.dropfolder.security.IIDSearchableUserDetailsManager;
import cz.cvut.felk.via.dropfolder.security.User;
import cz.cvut.felk.via.dropfolder.security.UserIDNotFoundException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.sql.DataSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * @author Revan
 */
public class DatabaseUsers implements IIDSearchableUserDetailsManager{

    @Resource
    private DataSource datasource;
    final String USERS_TABLE = "users";
    
    
    @Override
    public boolean userExists(String userName) {
        return loadUserInternal(userName, null) != null;
    }

    @Override
    public UserDetails loadUserById(int ID) throws UserIDNotFoundException {
        UserDetails ud = loadUserInternal(null, ID);
        
        if (ud == null) {
            throw new UserIDNotFoundException(ID);
        }
        
        return ud;
    }
    
    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        UserDetails ud = loadUserInternal(userName, null);
        
        if (ud == null) {
            throw new UsernameNotFoundException("User " + userName + " not found!");
        }
        
        return ud;
    }

    @Override
    public User createUser(String userName, String password) {
        String insert = "Insert Into " + USERS_TABLE + " (login, password)" +
                " VALUES ('" + userName + "','" + password +
                "');";
        
        try {
            Connection c = datasource.getConnection();
            Statement stmt = c.createStatement();
            stmt.executeUpdate(insert);
            c.close();
            return (User) loadUserByUsername(userName);
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        } 
    }

    @Override
    public void updateUser(UserDetails ud) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteUser(String userName) {
        String delete = "DELETE FROM " + USERS_TABLE + " WHERE login='" + userName + "';";
        
        try {
            Connection c = datasource.getConnection();
            Statement stmt = c.createStatement();
            stmt.executeUpdate(delete);
            c.close();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void changePassword(String oldPassword, String newPassword) {
        Authentication currentUser = SecurityContextHolder.getContext().getAuthentication();

        String userName = currentUser.getName();
        
        String update = "UPDATE " + USERS_TABLE + " SET password='" + newPassword
                + "' WHERE login='" + userName + "';";
        
        try {
            Connection c = datasource.getConnection();
            Statement stmt = c.createStatement();
            stmt.executeUpdate(update);
            c.close();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    private User loadUserInternal(String userName, Integer id) {
        String where = (userName == null) ? ("id='" + id + "';") : ("login='" + userName + "';");
        String select = "Select id,login,password from " + USERS_TABLE + " WHERE " + where;
        
        try {
            Connection c = datasource.getConnection();
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(select);
            
            if (!rs.next()) {
                return null;
            }
            
            int uId = rs.getInt("id");
            String uName = rs.getString("login");
            String uPass = rs.getString("password");
            c.close();
            return new User(uId, uName, uPass, new ArrayList<GrantedAuthority>());
            
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void createUser(UserDetails ud) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
