package cz.cvut.felk.via.dropfolder.filesystem;

import org.omg.CORBA.DynAnyPackage.Invalid;

/**
 * Descriptor for a file.
 *
 * @author Petr Kalivoda
 */
public class FileDescriptor implements IEntryDescriptor {
    
    private final int account_id;
    private String name;
    private final String hash;
    private final String contentType;
    private String parent;
    private final byte[] data;
    
    public FileDescriptor(int account_id, String name, String hash, String contentType, String parent, byte[] data) {
        this.account_id = account_id;
        this.name = name;
        this.hash = hash;
        this.contentType = contentType;
        this.parent = parent;
        this.data = data;
    }
    
    public void setName(String newName) {
        if(newName == null || newName.length() == 0) {
            //nothing to do here!
            return;
        }
        
        name = newName;
    }
    
    public void setParent(String newParent) {
        parent = newParent;
    }

    @Override
    public String getHash() {
        return hash;
    }

    @Override
    public EntryType getType() {
        return EntryType.FILE;
    }

    @Override
    public int getAccountId() {
        return account_id;
    }
    
    @Override
    public String getParent() {
        return parent;
    }

    @Override
    public String getName() {
        return name;
    }
    
    public String getContentType() {
        return contentType;
    }
    
    public byte[] getData() {
        return data;
    }

}
