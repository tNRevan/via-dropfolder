package cz.cvut.felk.via.dropfolder.exceptions;

/**
 * Exception for when attempting to non-recursively delete a non-empty directory
 *
 * @author Petr Kalivoda
 */
public class DirectoryNotEmpty extends Conflict {

    public DirectoryNotEmpty(String directoryHash) {
        super(String.format("Directory '%s' is not empty and therefore can't be deleted non-recursively.", directoryHash));
    }

}
