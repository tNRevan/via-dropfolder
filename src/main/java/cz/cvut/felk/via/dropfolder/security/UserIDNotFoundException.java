package cz.cvut.felk.via.dropfolder.security;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Exception for missing userids.
 * @author Petr Kalivoda
 */
public class UserIDNotFoundException extends UsernameNotFoundException {
    public UserIDNotFoundException(int ID) {
        super(String.format("User with ID %d was not found.", ID));
    }
}
