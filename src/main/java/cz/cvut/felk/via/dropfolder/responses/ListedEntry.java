package cz.cvut.felk.via.dropfolder.responses;

/**
 * Response for listed entry.
 * 
 * @author Petr Kalivoda
 */
public class ListedEntry implements IResponse{
    
    private final IFilesystemEntryResponse delegate;
    
    public ListedEntry(IFilesystemEntryResponse delegate) {
        this.delegate = delegate;
    }

    @Override
    public String getUrl() {
        return delegate.getUrl();
    }
    
    public String getName() {
        return delegate.getName();
    }
    
    public String getObjectName() {
        return delegate.getObjectName();
    }
    
    public String getType() {
        return delegate.getType();
    }
    
}
