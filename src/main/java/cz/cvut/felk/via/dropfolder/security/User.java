/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.cvut.felk.via.dropfolder.security;

import java.util.Collection;
import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author Petr Kalivoda
 */
public class User extends org.springframework.security.core.userdetails.User {
    
    private final int ID;
    protected String password;

    public User(int ID, String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
        this.ID = ID;
        this.password = password;
    }
    
    public int getID() {
        return ID;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    @Override
    public String getPassword() {
        return password;
    }
    
}
